# FredMod
A module based web framework for easily building online control panels etc.

### Applications
FredMod is used as a bases for FredMin, FreDAM and many other online control
panel solutions to easily add features, modules and plugins to online services.

Keeping everything modular with dynamic loading makes it easier for developers
to add or remove features without having to write a front end for each 
application.

### Screenshots

![Screenshot 1](images/screenshot-1.png)

![Screenshot 2](images/screenshot-2.png)

### Contribution
Standard rule apply. No 4 spaces, no tabs, understandable variable names and
most importantly, *Be excellent to each other!*.

<?php

// Get Public IP Address
$PublicIP = shell_exec("wget -qO- http://ipecho.net/plain | xargs echo");

// Client IP
$ClientIP = $_SERVER['REMOTE_ADDR'];

// Logged in user
$WhoAmI = shell_exec("whoami");

// Print SSH Command
PrintOS();
echo "Public IP: $PublicIP<br />";
echo "Client IP: $ClientIP<br />";
echo "Logged in Username: $WhoAmI";

// Print the usuage bar graphs
echo "<p>";
CpuUsage();
MemUsage();
DiskUsage();
echo "</p>";

// Ping Ports
PingIt('Apache', 80);

  /////////////////
 //  FUNCTIONS  //
/////////////////

// Print OS Type
function PrintOS() {
    $MachineName = shell_exec("cat /etc/os-release");

    // Cleanup Machine Name
    $MachineName = explode ("\n" , $MachineName);
    $MachineName = $MachineName[0];
    $MachineName = trim( $MachineName, "PRETTY_NAME=");
    $MachineName = trim( $MachineName, '"');

    echo "OS: $MachineName <br />";
}

function CpuUsage() {
    $Command = "grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}'";
    $Usage = shell_exec("$Command");

    // Print as Graph
    Grafit('CPU Usage', $Usage, 100);
}

function MemUsage() {
    $Command = "cat /proc/meminfo";
    $Usage = shell_exec("$Command");

    // Clean up
    $Usage = preg_replace('!\s+!', '¬', $Usage);
    $Usage = explode('¬', $Usage);
    
    // $Usage = explode("\n\r", $Usage);
    $Free = $Usage[7];
    $Total = $Usage[1];
    $Used = $Total - $Free;

    // Print as Graph
    Grafit('Memory Usage', $Used, $Total);
}

function DiskUsage() {
    $Command = "df /";
    $Usage = shell_exec("$Command");

    // Clean up
    $Usage = preg_replace('!\s+!', '¬', $Usage);
    $Usage = explode('¬', $Usage);
    
    // $Usage = explode("\n\r", $Usage);
    $Used = $Usage[11];
    $Used = trim($Used, '%');

    // Print as Graph
    Grafit('Disk Usage', $Used, 100);
}

// Pings the port, to see if your... Still Alive ->
function PingIt($PortName, $PortNum) {
    $PingIt = fsockopen('localhost', $PortNum);

    if ($PingIt) {
        echo "
        <div style='color:green;'>
        $PortName $PortNum: Online
        </div>";
    }

    else {
        echo "
        <div style='color:red;'>
        $PortName $PortNum: No Connection
        </div>";
    }
}   

// Bar Graph Generator
function Grafit ($Name, $value,$total) {

    $barwidth = 200;
    $barheight = 18;

    // Calculates bargraph dimensions
    $ratio = $barwidth/$total;
    $barvaluewidth = $ratio*$value;

    // Calculate percentage for else statement
    $Percentage = (100/$total)*$value;
    $Percentage = round($Percentage); // Fix the float

    // Uses floor to calculate nearest division
    $colours = array("#D2FF1A","#FFC52C","#FF7D2C","#FF042C");
    $barcolour = $colours[floor($Percentage / 25)];

    echo "
        <div 
            style='
            display: inline-block;
            text-align: right;
            padding: 0;
            width: 150px;'>
            $Name
        </div>

        <div style='
            display:inline-block;
            height: $barheight;
            width: $barwidth;'>
                <div style='
                    background-color: $barcolour;
                    height: $barheight;
                    width: $barvaluewidth;'>
                </div>
        </div>

        <div style='display: inline-block;'>$Percentage %</div>
        <br />
    ";
}
?>
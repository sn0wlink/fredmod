 <?php

 /* 
    System Stats
    ----------------------------
    Date: 07/06/2020
    Author: David Collins-Cubitt
 */

// Pull main config files
include ('../../../config.php'); // FredMod Config
include ('../../../functions.php'); // FredMod Functions

// Include Application Config
include ('app-config.php');

// Build page layout
PageHeader();

include("stats.php");

// Build page footer
PageFooter();

?> 


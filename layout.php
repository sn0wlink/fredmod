<html>
<head>
    <link rel='icon' href='favicon.ico' type='image/x-icon' />
    <link rel='stylesheet' href='style.css' type='text/css'>
    <title>FredMod - <?= $SiteName; ?></title>
</head>

<body>
    <div class='header'>
        <img class='logo' src='images/header.png'>
        <center><h3><?= $SiteName ?></h3></center>
    </div>

    <div class='content'>

<!-- 
    Standard Modules
    <p class='menu-header'>
        Standard Modules (Stable)
    </p>
!-->

    <?php BuildModules(); ?>

    </div>

    <div class='footer'>
        &copy; 2020 Fredmod Framework - Created by David Collins-Cubitt
    </div>
</body>

</html>